use actix_web::{
    dev::ServiceRequest, error, middleware::Logger, post, web, App, Error, HttpRequest,
    HttpResponse, HttpServer, Responder,
};
use actix_web_httpauth::extractors::basic::BasicAuth;
use actix_web_httpauth::extractors::basic::Config;
use actix_web_httpauth::extractors::AuthenticationError;
use actix_web_httpauth::middleware::HttpAuthentication;
use anyhow::{Context, Result};
use keycloak::{KeycloakAdmin, KeycloakAdminToken};
use log::{debug, error, warn};
use regex::Regex;
use serde::Deserialize;
use std::fs;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::{collections::HashMap, fs::File};
use std::{net::SocketAddr, time::Duration};
use structopt::StructOpt;
use tokio::sync::Mutex;
use url::Url;

#[derive(StructOpt, Clone, Debug)]
#[structopt(
    name = "mail-credential-syncer",
    author,
    about,
    global_settings = &[structopt::clap::AppSettings::ColoredHelp],
)]
struct CliConfig {
    /// Local socket address to listen on
    #[structopt(long)]
    listen: SocketAddr,

    #[structopt(long, env, default_value = "120")]
    /// Time between full sync in seconds
    sync_interval: u64,

    #[structopt(long, env, default_value = "12")]
    /// The allowed bcrypt cost
    bcrypt_cost: u8,

    /// Username for basic auth of HTTP endpoint
    #[structopt(long, env)]
    basic_auth_username: String,

    /// Password for basic auth of HTTP endpoint
    #[structopt(long, env)]
    basic_auth_password: String,

    /// Path to file containing Keycloak UUID to mail address mappings
    #[structopt(long)]
    mailmap: PathBuf,

    /// Path to write Dovecot passwd-file file
    #[structopt(long)]
    dovecot_passwd_file: PathBuf,

    /// Path to write OpenSMTPD creds file
    #[structopt(long)]
    opensmtpd_creds: PathBuf,

    /// Path to a post-sync hook executable
    #[structopt(long)]
    post_sync_hook: PathBuf,

    /// Keycloak URL for incoming webhook validation and periodic syncs
    #[structopt(long)]
    keycloak_url: Url,

    /// Keycloak realm
    #[structopt(long, env)]
    keycloak_realm: String,

    /// Keycloak client id
    #[structopt(long, env)]
    keycloak_client_id: String,

    /// Keycloak client secret
    #[structopt(long, env)]
    keycloak_client_secret: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
#[derive(Debug)]
struct Representation {
    id: String,
}

// Only the subset we are interested in:
// https://github.com/keycloak/keycloak/blob/master/server-spi-private/src/main/java/org/keycloak/events/Event.java
// https://github.com/keycloak/keycloak/blob/master/server-spi-private/src/main/java/org/keycloak/events/admin/AdminEvent.java
#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
#[derive(Debug)]
struct Event {
    operation_type: String,
    resource_type: String,
    representation: Representation,
}

#[derive(Debug)]
struct Mailbox {
    email_address: String,
    password_hash: Option<String>,
}

impl Mailbox {
    // https://doc.dovecot.org/configuration_manual/authentication/passwd_file
    fn to_dovecot_passwd(&self) -> String {
        format!(
            "{}:{}::::::",
            self.email_address,
            self.password_hash.as_ref().unwrap_or(&String::new())
        )
    }

    fn to_opensmtpd_creds(&self) -> Option<String> {
        if let Some(password_hash) = &self.password_hash {
            Some(format!("{} {}", self.email_address, password_hash))
        } else {
            None
        }
    }
}

/// Get the user's password from Keycloak and store it in the mailbox
async fn sync_user(mailbox: &mut Mailbox, id: &str, args: &CliConfig) -> Result<()> {
    let client = reqwest::Client::new();
    let url = &args.keycloak_url;
    let url = &format!(
        "{}://{}:{}",
        url.scheme(),
        url.host_str().unwrap_or("localhost"),
        url.port().unwrap_or(80)
    );
    let admin_token = KeycloakAdminToken::acquire(
        url,
        &args.keycloak_client_id,
        &args.keycloak_client_secret,
        &client,
    )
    .await?;

    let admin = KeycloakAdmin::new(url, admin_token, client);
    let user = admin.user_get(&args.keycloak_realm, id).await?;
    // If the user removes the attribute or the hash is invalid we want the password removed
    mailbox.password_hash = None;
    if let Some(mail_password_hash) = user
        .attributes
        .as_ref()
        .and_then(|hash| hash.get("mail_password_hash"))
        .and_then(|mail_password_hash_array| mail_password_hash_array.get(0))
        .and_then(|mail_password_hash| mail_password_hash.as_str())
    {
        // We only allow bcrypt with cost args.bcrypt_cost
        let regex = [
            r"^\$2a\$",
            &args.bcrypt_cost.to_string(),
            r"\$[A-Za-z0-9]{53}$",
        ]
        .concat();
        let re = Regex::new(&regex).unwrap();
        if re.captures(mail_password_hash).is_some() {
            mailbox.password_hash = Some(mail_password_hash.to_string());
        } else {
            warn!("Received hash but it has wrong format");
            debug!("Hash in question is {:#}", mail_password_hash);
        }
    }

    Ok(())
}

/// Write Dovecot and OpenSMTPD configs
async fn write_configs(mailboxes: &HashMap<String, Mailbox>, args: &CliConfig) -> Result<()> {
    let dovecot_passwd_file_tmp_path = args.dovecot_passwd_file.with_extension("tmp");
    let dovecot_passwd_file_old_path = args.dovecot_passwd_file.with_extension("old");
    let opensmtpd_creds_tmp_path = args.opensmtpd_creds.with_extension("tmp");
    let opensmtpd_creds_old_path = args.opensmtpd_creds.with_extension("old");

    let mut dovecot_passwd_file = File::create(&dovecot_passwd_file_tmp_path)?;
    let mut opensmtpd_creds = File::create(&opensmtpd_creds_tmp_path)?;
    debug!("writing configs");
    for (_, mailbox) in mailboxes.iter() {
        writeln!(dovecot_passwd_file, "{}", mailbox.to_dovecot_passwd())?;
        if let Some(opensmtpd_creds_content) = &mailbox.to_opensmtpd_creds() {
            writeln!(opensmtpd_creds, "{}", opensmtpd_creds_content)?;
        }
    }

    if Path::new(&args.dovecot_passwd_file).exists() {
        fs::copy(&args.dovecot_passwd_file, &dovecot_passwd_file_old_path)?;
    }
    if Path::new(&args.opensmtpd_creds).exists() {
        fs::copy(&args.opensmtpd_creds, &opensmtpd_creds_old_path)?;
    }

    fs::rename(&dovecot_passwd_file_tmp_path, &args.dovecot_passwd_file)?;
    fs::rename(&opensmtpd_creds_tmp_path, &args.opensmtpd_creds)?;

    // https://doc.rust-lang.org/std/process/struct.Command.html#platform-specific-behavior
    let status = Command::new(&args.post_sync_hook).status();
    if (status.is_err() || !status.unwrap().success())
        && Path::new(&dovecot_passwd_file_old_path).exists()
        && Path::new(&opensmtpd_creds_old_path).exists()
    {
        debug!("reverting configs");
        fs::rename(&dovecot_passwd_file_old_path, &args.dovecot_passwd_file)?;
        fs::rename(&opensmtpd_creds_old_path, &args.opensmtpd_creds)?;
        Command::new(&args.post_sync_hook)
            .args(&["failed"])
            .status()?;
    }

    Ok(())
}

/// Get all the users' passwords from Keycloak and write_configs
async fn full_sync(mailboxes: &mut HashMap<String, Mailbox>, args: &CliConfig) -> Result<()> {
    debug!("dict {:?}", &mailboxes);
    for (id, mut mailbox) in mailboxes.iter_mut() {
        sync_user(&mut mailbox, id, &args)
            .await
            .with_context(|| format!("Failed to sync user {}", id))?;
    }
    write_configs(mailboxes, args)
        .await
        .context("Error writing configs")?;

    Ok(())
}

/// Load mailmap file from disk
fn load_mailmap(mailmap_file: &PathBuf) -> Result<HashMap<String, Mailbox>> {
    let mailmap_content =
        fs::read_to_string(mailmap_file).context("Loading mailmap file failed")?;
    let mailmap: HashMap<String, String> =
        serde_json::from_str(&mailmap_content).context("Can't parse JSON from mailmap")?;

    let mut mailboxes = HashMap::<String, Mailbox>::new();
    for (id, email_address) in &mailmap {
        mailboxes.insert(
            id.to_string(),
            Mailbox {
                email_address: email_address.to_string(),
                password_hash: None,
            },
        );
    }
    Ok(mailboxes)
}

/// Custom json error handler
fn json_error_handler(err: error::JsonPayloadError, _req: &HttpRequest) -> error::Error {
    use actix_web::error::JsonPayloadError;

    let detail = err.to_string();
    let resp = match &err {
        JsonPayloadError::ContentType => HttpResponse::UnsupportedMediaType().body(detail),
        JsonPayloadError::Deserialize(json_err) if json_err.is_data() => {
            HttpResponse::UnprocessableEntity().body(detail)
        }
        _ => HttpResponse::BadRequest().body(detail),
    };
    error::InternalError::from_response(err, resp).into()
}

/// Webhook endpoint used by Keycloak event listener SPI: https://github.com/svenstaro/keycloak-http-webhook-provider
#[post("/webhook")]
async fn webhook(
    event: web::Json<Event>,
    mailboxes: web::Data<Mutex<HashMap<String, Mailbox>>>,
    args: web::Data<CliConfig>,
) -> impl Responder {
    debug!("Received\n{:?}", event);

    if event.operation_type == "UPDATE" && event.resource_type == "USER" {
        let mut mailboxes = mailboxes.lock().await;

        if let Some(mut mailbox) = mailboxes.get_mut(&event.representation.id) {
            if sync_user(&mut mailbox, &event.representation.id.clone(), &args)
                .await
                .is_err()
            {
                return HttpResponse::InternalServerError();
            }
            if write_configs(&mailboxes, &args).await.is_err() {
                return HttpResponse::InternalServerError();
            }
        } else {
            return HttpResponse::NotFound();
        }
    }

    HttpResponse::Ok()
}

/// Check username/password (HTTP basic auth)
async fn http_auth_validator(
    req: ServiceRequest,
    credentials: BasicAuth,
) -> Result<ServiceRequest, Error> {
    if let Some(args) = req.app_data::<web::Data<CliConfig>>() {
        if credentials.user_id() == &args.basic_auth_username
            && credentials.password().is_some()
            && credentials.password().unwrap() == &args.basic_auth_password
        {
            return Ok(req);
        }
    }
    let config = req.app_data::<Config>().cloned().unwrap_or_default();

    Err(AuthenticationError::from(config).into())
}

#[actix_web::main]
async fn main() -> Result<()> {
    std::env::set_var("RUST_LOG", "mail_credential_syncer=debug,actix_web=info");
    pretty_env_logger::init();

    let args = CliConfig::from_args();

    let mut mailboxes = load_mailmap(&args.mailmap)?;
    // Do a initial sync, so we know "mailboxes" is in-sync. If we don't we risk writing configs lacking passwords.
    full_sync(&mut mailboxes, &args).await?;
    let mailboxes = web::Data::new(Mutex::new(mailboxes));
    {
        let args = args.clone();
        let mailboxes = mailboxes.clone();
        // We do a full_sync every sync_interval and we sync individual
        // users when they change their password (see the webhook function)
        actix_rt::spawn(async move {
            let mut interval = actix_rt::time::interval(Duration::from_secs(args.sync_interval));
            loop {
                debug!("full sync");
                let mut mailboxes = mailboxes.lock().await;
                if let Err(err) = full_sync(&mut mailboxes, &args).await {
                    error!("Error doing full sync: {}", err);
                }
                // Drop the mutex lock before waiting for next sync
                drop(mailboxes);
                interval.tick().await;
            }
        });
    }
    {
        let args2 = args.clone();
        HttpServer::new(move || {
            let auth = HttpAuthentication::basic(http_auth_validator);
            App::new()
                .app_data(mailboxes.clone())
                .app_data(web::JsonConfig::default().error_handler(json_error_handler))
                .app_data(web::Data::new(args2.clone()))
                .wrap(Logger::default())
                .wrap(auth)
                .service(webhook)
        })
        .bind(args.listen)?
        .run()
        .await
        .context("Failed to run actix")
    }
}
