# Mail Credential Syncer

Small tool to sync mail passwords from Keycloak to Dovecot

## How to run

First of all, we need to start a pre-configured Keycloak server to run our tests against:

    git clone https://github.com/svenstaro/keycloak-http-webhook-provider.git
    cd keycloak-http-webhook-provider
    mvn clean install
    keycloak/run-keycloak-container.sh

Once Keycloak is running, login and get the id of a user and update `mailmap.json` accordingly, afterwards we can start our webhook server:

    cargo run -- --listen 0.0.0.0:5000 --mailmap mailmap.json --keycloak-url http://127.0.0.1:8080 --keycloak-client-id admin --keycloak-client-secret admin --keycloak-realm master --dovecot-passwd-file passwd-file --opensmtpd-creds creds --basic-auth-password auth_password --basic-auth-username auth_user --post-sync-hook /bin/true

Now go to your Keycloak server at http://localhost:8080, login via admin/admin and add a new user attribute called `mail_password_hash` (with a bcrypt hash) to a user of your choice. The webhook server should then give you some output of what's happening.
